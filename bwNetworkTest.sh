#!/bin/bash

source="$1"
dest="$2"
destip="$3"

# TODO 


if [[ -z "$source" || -z "$dest" ]];
then
  echo "One or more inputs are empty"
  echo "linux package nc (netcat) required"
  echo "Usage: ./bwNetworkTest.sh [Source Server Type] [Destination Server Type] [Destination Server IP]"
  echo "Example ./bwNetworkTest.sh AS NS 192.168.1.11"

elif [[ "$source" == "AS" && "$dest" == "NS" ]];
then
  echo "Scanning required AS > NS ports..."
  for p in 80 443 2220 5060 5090
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "NS" && "$dest" == "AS" ]];
then
  echo "Scanning required NS > AS ports..."
  for p in 80 443 2220 5060 5090
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "NS" && "$dest" == "NS" ]]
then
  echo "Scanning required NS > NS ports..."
  for p in 22 11201 11203 2223 5090
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "AS" && "$dest" == "AS" ]]
then
  echo "Scanning required AS > AS ports..."
  for p in 22 2220 2223 5015 5060 5090 11201 11203
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "MS" && "$dest" == "AS" ]]
then
  echo "Scanning required MS > AS ports..."
  for p in 80 5060 7575
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "NS" && "$dest" == "XSP" ]]
then
  echo "Scanning required NS > XSP ports..."
    for p in 80 443 2220 5060 5090
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

  nc -nvz "$destip" 443
  nc -nvz "$destip" 80

elif [[ "$source" == "AS" && "$dest" == "MS" ]]
then
  echo "Scanning required AS > MS ports..."
  for p in 80 5060 7575
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "AS" && "$dest" == "PS" ]]
then
  echo "Scanning required AS > PS ports..."
  for p in 80 443
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "AS" && "$dest" == "XSP" ]]
then
  echo "Scanning required AS > XSP ports..."
for p in 80 443 2220 2221 2320 2321
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "XSP" && "$dest" == "AS" ]]
then
  echo "Scanning required XSP > AS ports..."
for p in 80 443 2220 2221 2320 2321
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

elif [[ "$source" == "XSP" && "$dest" == "NS" ]]
then
  echo "Scanning required XSP > NS ports..."
  for p in 80 2220 2320
  do
    echo "scanning $p"
    nc -nvz "$destip" $p
  done

else
  echo "linux package nc (netcat) required"
  echo "Usage: ./bwNetworkTest.sh [Source Server Type] [Destination Server Type] [Destination Server IP]"
  echo "Example ./bwNetworkTest.sh AS NS 192.168.1.11"    
fi

#tac $source.txt
#rm -rf $source.txt

#for server in `more server-list.txt`
#do
#for port in `more port-list.txt`
#do
#echo $server
#nc -zvw3 $server $port
